#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/UInt16.h"
#include "std_msgs/Int32.h"
#include <cmath>
#include "std_msgs/Bool.h"

#define PIE atan(1)*4

int current_task = 1;

/*
1. Gen_traj receptacle (0,1.6135, 90deg)
2. driving to receptacle
3. pick up
4. gen_traj dispenser
5. driving to dispenser
6. dispensing 
*/

void TOFDistanceSubscriberCallback(const std_msgs::UInt16& msg)
{
    //Detection range 
	if (current_task == 0 && msg.data > 30) { //Run for the first time when at starting point
	    current_task = 1;
	} else if (msg.data <= 30) { //when at receptacle, want to stop
	    current_task = 4;
	}
}

void Task_1_done_callback(const std_msgs::Bool& msg)
{
	if (msg.data) {
	    ROS_INFO_STREAM("TASK 1 DONE, GO TO 2");
	    current_task = 2;
	}
}

void Task_2_done_callback(const std_msgs::Bool& msg)
{
	if (msg.data) {
	    ROS_INFO_STREAM("TASK 2 DONE, GO TO 3");
	    current_task = 3;
	}
}

void Task_3_done_callback(const std_msgs::Bool& msg)
{
	if (msg.data) {
	    ROS_INFO_STREAM("TASK 3 DONE, GO TO 4");
	    current_task = 4;
	}
}

void Task_4_done_callback(const std_msgs::Bool& msg)
{
	if (msg.data) {
	    ROS_INFO_STREAM("TASK 4 DONE, GO TO 5");
	    current_task = 5;
	}
}

void Task_5_done_callback(const std_msgs::Bool& msg)
{
	if (msg.data) {
	    ROS_INFO_STREAM("TASK 5 DONE, GO TO 6");
	    current_task = 6;
	}
}

void Task_6_done_callback(const std_msgs::Bool& msg)
{
	if (msg.data) {
	    ROS_INFO_STREAM("TASK 6 DONE, GO TO 7");
	    current_task = 7;
	}
}

void Task_7_done_callback(const std_msgs::Bool& msg)
{
	if (msg.data) {
	    ROS_INFO_STREAM("TASK 7 DONE, GO TO 8");
	    current_task = 8;
	}
}

void Task_8_done_callback(const std_msgs::Bool& msg)
{
	if (msg.data) {
	    ROS_INFO_STREAM("TASK 8 DONE, GO TO 1");
	    current_task = 1;
	}
}


int main(int argc, char* argv[])
{
	// Initialise the node
	ros::init(argc, argv, "brain");
	ros::NodeHandle nodeHandle("~");
	ros::NodeHandle GlobalNodeHandle("/global");
	// Initialise a publisher
	//ros::Publisher current_thetas = GlobalNodeHandle.advertise<motor_ctrl>("current_thetas", 10, false);
	ros::Publisher current_task_pub = GlobalNodeHandle.advertise<std_msgs::Int32>("current_task",10,false);
	// Initialise a subscriber
	ros::Subscriber task1done = GlobalNodeHandle.subscribe("task_1_done", 1, Task_1_done_callback);
	ros::Subscriber task2done = GlobalNodeHandle.subscribe("task_2_done", 1, Task_2_done_callback);
	ros::Subscriber task3done = GlobalNodeHandle.subscribe("task_3_done", 1, Task_3_done_callback);
	ros::Subscriber task4done = GlobalNodeHandle.subscribe("task_4_done", 1, Task_4_done_callback);
	ros::Subscriber task5done = GlobalNodeHandle.subscribe("task_5_done", 1, Task_5_done_callback);
	ros::Subscriber task6done = GlobalNodeHandle.subscribe("task_6_done", 1, Task_6_done_callback);
    ros::Subscriber task7done = GlobalNodeHandle.subscribe("task_7_done", 1, Task_7_done_callback);
    ros::Subscriber task8done = GlobalNodeHandle.subscribe("task_8_done", 1, Task_8_done_callback);
    // ros::Subscriber task9done = GlobalNodeHandle.subscribe("task_9_done", 1, Task_9_done_callback);
	ros::Subscriber tof_distance_subscriber = GlobalNodeHandle.subscribe("tof_distance", 1, TOFDistanceSubscriberCallback);

// 	ros::Subscriber set_motor_duty_cycle_subscriber3 = NH3.subscribe("gpio_event_3", 1, templateSubscriberCallback3);
//     ros::Subscriber set_motor_duty_cycle_subscriber4 = NH4.subscribe("gpio_event_4", 1, templateSubscriberCallback4);

    // timer_for_odometry = nodeHandle.createTimer(ros::Duration(0.1), timerCallbackForOdometry, true);
    //timer_for_motor_ctrl = nodeHandle.createTimer(ros::Duration(0.01), timerCallbackForMotorCtrl, true);


	//ros::Rate loop_rate(50);
	
	// Enter a loop that continues while ROS is still running
	while (ros::ok())
	{
	    std_msgs::Int32 msg;
	    msg.data = current_task;
	    current_task_pub.publish(msg);
        
        //ROS_INFO_STREAM("THIS IS ULAN");
		// Spin once so that this node can service any
		// callbacks that this node has queued.
// 		motor_ctrl msg = motor_ctrl();
// 		msg.theta_r_dot = Theta_r_dot;
// 		msg.theta_l_dot = Theta_l_dot;
// 		current_thetas.publish(msg);

		
		ros::spinOnce();

		// Sleep for the specified loop rate
		//loop_rate.sleep();
	} // END OF: "while (ros::ok())"


	return 0;
}
