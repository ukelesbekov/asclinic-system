// Copyright (C) 2021, The University of Melbourne, Department of Electrical and Electronic Engineering (EEE)
//
// This file is part of ASClinic-System.
//
// See the root of the repository for license details.
//
// ----------------------------------------------------------------------------
//     _    ____   ____ _ _       _          ____            _
//    / \  / ___| / ___| (_)____ (_) ___    / ___| _   _ ___| |_ ___ ________
//   / _ \ \___ \| |   | | |  _ \| |/ __|___\___ \| | | / __| __/ _ \  _   _ \
//  / ___ \ ___) | |___| | | | | | | (_|_____|__) | |_| \__ \ ||  __/ | | | | |
// /_/   \_\____/ \____|_|_|_| |_|_|\___|   |____/ \__, |___/\__\___|_| |_| |_|
//                                                 |___/
//
// DESCRIPTION:
// Template node for polling the value of a GPIO pin at a fixed frequency
//
// ----------------------------------------------------------------------------





#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/Int32.h"

#include <gpiod.h>
#include "asclinic_pkg/motor_ctrl.h"
#include "asclinic_pkg/error_msg.h"
using namespace asclinic_pkg;

ros::Timer timer_for_error_computation;
ros::Publisher error_computation_publisher;

float Theta_r_dot_err, Theta_l_dot_err;
float x[4] = {0,0,0,0};

/* Respond to subscriber receiving a message */
void templateSubscriberCallback1(const motor_ctrl& msg)
{
	//ROS_INFO_STREAM("[ERROR_COMPUTATION] Message received Thetha_dot_r_ref: " << msg.theta_dot_r_ref << "     Thetha_dot_l_ref: " << msg.theta_dot_l_ref);
	x[0] = msg.theta_dot_r_ref;
	x[1] = msg.theta_dot_l_ref;
}

/* Respond to subscriber receiving a message */
void templateSubscriberCallback2(const motor_ctrl& msg)
{
	//ROS_INFO_STREAM("[ERROR_COMPUTATION] Message received Thetha_dot_r: " << msg.theta_r_dot << "     Thetha_dot_l: " << msg.theta_l_dot);
	x[2] = msg.theta_r_dot;
	x[3] = msg.theta_l_dot;
}

void timerCallbackForErrorComputation(const ros::TimerEvent&)
{
	// Declare a static integer for publishing a different
	// number each time
	static uint counter = 0;
	// Increment the counter
	counter++;
    
    Theta_r_dot_err = x[0]-x[2];
    Theta_l_dot_err = x[1]-x[3];
    
    //ROS_INFO_STREAM("[ERROR_COMPUTATION] Theta_r_dot_err: " << Theta_r_dot_err << "     Theta_l_dot_err: " << Theta_l_dot_err);
    
    
    error_msg msg = error_msg();
    msg.theta_dot_r_err = Theta_r_dot_err;
	msg.theta_dot_l_err = Theta_r_dot_err;
    
    error_computation_publisher.publish(msg);
    
	// START THE TIMER AGAIN
	// > Stop any previous instance that might still be running
	timer_for_error_computation.stop();
	// > Set the period again (second argument is reset)
	timer_for_error_computation.setPeriod( ros::Duration(0.001), true);
	// > Start the timer again
	timer_for_error_computation.start();
}

int main(int argc, char* argv[])
{
	// Initialise the node
	ros::init(argc, argv, "error_computation");
	ros::NodeHandle nodeHandle("~");
	ros::NodeHandle GlobalNodeHandle("/global");
	// Initialise a publisher
	error_computation_publisher = GlobalNodeHandle.advertise<error_msg>("error_computation", 10, false);
	// Initialise a subscriber
	// > Note that the subscriber is included only for the purpose
	//   of demonstrating this template node running stand-alone
	ros::Subscriber error_computation_subscriber1 = GlobalNodeHandle.subscribe("gpio_event_motor", 1, templateSubscriberCallback1);
	ros::Subscriber error_computation_subscriber2 = GlobalNodeHandle.subscribe("current_thetas", 1, templateSubscriberCallback2);
    timer_for_error_computation = nodeHandle.createTimer(ros::Duration(0.001), timerCallbackForErrorComputation, true);
    
	// Initialise a variable with loop rate for
	// polling the GPIO pin
	// > Input argument is the frequency in hertz, as a double
	//ros::Rate loop_rate(100);
	ros::spin();
// 	while (ros::ok())
// 	{
//     	error_msg msg = error_msg();
// 		msg.theta_dot_r_err = Theta_r_dot_err;
// 		msg.theta_dot_l_err = Theta_r_dot_err;
// 		error_computation_publisher.publish(msg);
// 		ros::spinOnce();
// 		loop_rate.sleep();
// 	}

	return 0;
}
