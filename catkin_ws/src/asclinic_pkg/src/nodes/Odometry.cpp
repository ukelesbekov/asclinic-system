#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/UInt16.h"
#include "std_msgs/Int32.h"
#include <cmath>
#include "i2c_driver/i2c_driver.h"

#include "pololu_smc_g2/pololu_smc_g2.h"

#include <bitset>
#include "asclinic_pkg/encoder_msg.h"
#include "asclinic_pkg/motor_ctrl.h"
#include "asclinic_pkg/pose_msg.h"
using namespace asclinic_pkg;

#define PIE atan(1)*4

int x[4] = {0,0,0,0};
float pose[3] = {0,0,PIE/2};
float tim = 0;
int eve = 0;
float Theta_r = 0, Theta_l = 0;
float Theta_r_dot, Theta_l_dot;

ros::Timer timer_for_odometry;
ros::Timer timer_for_motor_ctrl;
ros::Publisher pose_publisher;

void timerCallbackForOdometry(const ros::TimerEvent&)
{

    float delta_s = 0.05/2*(Theta_l+Theta_r);
    float delta_phi = 0.05/0.25*(Theta_r-Theta_l);
    
    pose[0] = pose[0] + delta_s*cos(pose[2]+1/2*delta_phi);
    pose[1] = pose[1] + delta_s*sin(pose[2]+1/2*delta_phi);
    pose[2] = pose[2] + delta_phi;
    Theta_l = 0;
    Theta_r = 0;

    pose_msg pose_message = pose_msg();
    pose_message.x_p = pose[0];
    pose_message.y_p = pose[1];
    pose_message.phi = pose[2];
    pose_publisher.publish(pose_message);
	
	timer_for_odometry.stop();
	timer_for_odometry.setPeriod( ros::Duration(0.01), true);
	timer_for_odometry.start();
}



void timerCallbackForMotorCtrl(const ros::TimerEvent&)
{
	// Declare a static integer for publishing a different
	// number each time
	static uint counter = 0;
	// Increment the counter
	counter++;
	
//     static float buffer_R[10] = {0,0,0,0,0,0,0,0,0,0};//,0};//,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
//     static float buffer_L[10] = {0,0,0,0,0,0,0,0,0,0};//{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
//     float sum_R = 0, sum_L = 0;
    
     Theta_r_dot = Theta_r/0.01;
     Theta_l_dot = Theta_l/0.01;
// 	//ROS_INFO_STREAM(" LEFT WHEEL SPEED: " << Theta_l_dot << "      RIGHT WHEEL SPEED: " << Theta_r_dot);
// 	//ROS_INFO_STREAM(Theta_l_dot << " " << Theta_r_dot << " " << counter*0.1);
// 	//ROS_INFO_STREAM(Theta_l_dot << ", " << Theta_r_dot << ", " << counter*0.1);
//     for (int i=10-1; i>0; i--) {
// 			buffer_R[i] = buffer_R[i-1];
// 		    }
// 	buffer_R[0] = Theta_r_dot;
	
// 	for (int i=10-1; i>0; i--) {
// 			buffer_L[i] = buffer_L[i-1];
// 		    }
// 	buffer_L[0] = Theta_l_dot;
    
//     for (int i = 0; i<10; i++) {
//         sum_R += buffer_R[i];
//         sum_L += buffer_L[i];
//     }
//     Theta_r_dot = sum_R/10;
//     Theta_l_dot = sum_L/10;
    //ROS_INFO_STREAM(Theta_l_dot << " " << Theta_r_dot << " " << counter*0.01);
    
    //ROS_INFO_STREAM(" TIME IS:   " << 0.1*counter);
    Theta_l = 0;
    Theta_r = 0;
    
	// START THE TIMER AGAIN
	// > Stop any previous instance that might still be running
	timer_for_motor_ctrl.stop();
	// > Set the period again (second argument is reset)
	timer_for_motor_ctrl.setPeriod( ros::Duration(0.01), true);
	// > Start the timer again
	timer_for_motor_ctrl.start();
}


void templateSubscriberCallback1(const encoder_msg& msg)
{
	//ROS_INFO_STREAM("DATA RECEIVED FROM 1  (DATA IS)" << msg.event);
	//ROS_INFO_STREAM("DATA RECEIVED FROM 1  (TIME IS)  " << msg.time_of_event);
    tim+=msg.time_of_event;
    ///eve+=1;
	int rec = msg.event;
	x[0] = rec;
	//ROS_INFO_STREAM("ELAPLSED TIME: " << tim << "    COUNTS: " << eve);
}
void templateSubscriberCallback2(const encoder_msg& msg)
{
	//ROS_INFO_STREAM("DATA RECEIVED FROM 2  (DATA IS)" << msg.event);
	//ROS_INFO_STREAM("DATA RECEIVED FROM 2  (TIME IS)  " << msg.time_of_event);
    
	int rec = msg.event;
	x[1] = rec;
}
void templateSubscriberCallback3(const encoder_msg& msg)
{
	//ROS_INFO_STREAM("DATA RECEIVED FROM 3  (DATA IS)" << msg.event);
	//ROS_INFO_STREAM("DATA RECEIVED FROM 3  (TIME IS)  " << msg.time_of_event);
    
	int rec = msg.event;
	x[2] = rec;
}
void templateSubscriberCallback4(const encoder_msg& msg)
{
	//ROS_INFO_STREAM("DATA RECEIVED FROM 4  (DATA IS)" << msg.event);
	//ROS_INFO_STREAM("DATA RECEIVED FROM 4  (TIME IS)  " << msg.time_of_event);
    
	int rec = msg.event;
	x[3] = rec;
}



int main(int argc, char* argv[])
{
	// Initialise the node
	ros::init(argc, argv, "Odometry");
	ros::NodeHandle nodeHandle("~");
	ros::NodeHandle GlobalNodeHandle("/global");
	ros::NodeHandle NH1("/event1");
	ros::NodeHandle NH2("/event2");
	ros::NodeHandle NH3("/event3");
	ros::NodeHandle NH4("/event4");
	// Initialise a publisher
	//ros::Publisher current_thetas = GlobalNodeHandle.advertise<motor_ctrl>("current_thetas", 10, false);
	pose_publisher = GlobalNodeHandle.advertise<pose_msg>("current_pose",10,false);
	// Initialise a subscriber
	ros::Subscriber set_motor_duty_cycle_subscriber1 = NH1.subscribe("gpio_event_1", 100, templateSubscriberCallback1);
	ros::Subscriber set_motor_duty_cycle_subscriber2 = NH2.subscribe("gpio_event_2", 100, templateSubscriberCallback2);
	ros::Subscriber set_motor_duty_cycle_subscriber3 = NH3.subscribe("gpio_event_3", 100, templateSubscriberCallback3);
    ros::Subscriber set_motor_duty_cycle_subscriber4 = NH4.subscribe("gpio_event_4", 100, templateSubscriberCallback4);

    timer_for_odometry = nodeHandle.createTimer(ros::Duration(0.01), timerCallbackForOdometry, true);
    //timer_for_motor_ctrl = nodeHandle.createTimer(ros::Duration(0.01), timerCallbackForMotorCtrl, true);
	// Initialise a variable with loop rate for
	// polling the sensors
	// > Input argument is the frequency in hertz, as a double
	//ros::Rate loop_rate(50);
	
	int state_R = 0;
	int state_R_prev;
	int state_L = 0;
	int state_L_prev;
	int ctr = 0;

	while (ros::ok())
	{
	
	    int wheel_R_A = x[0];
	    int wheel_R_B = x[1];
	    int wheel_L_A = x[2];
	    int wheel_L_B = x[3];
        int wheel_R[2] = {wheel_R_A, wheel_R_B};
        int wheel_L[2] = {wheel_L_A, wheel_L_B};
        
        if (wheel_R_A == 0 && wheel_R_B == 0) {
            if (state_R == 2) {
                Theta_r = Theta_r + 2*PIE/800;
            }
            if (state_R == 4) {
                Theta_r = Theta_r - 2*PIE/800;
            }
            state_R = 1;
        } 
        else if (wheel_R_A == 1 && wheel_R_B == 0) {
            if (state_R == 1) {
            }
            if (state_R == 3) {
            }
            state_R = 2;
        }
        else if (wheel_R_A == 1 && wheel_R_B == 1) {
            if (state_R == 2) {
            }
            if (state_R == 4) {
            }
            state_R = 3;
        }
        else {
            if (state_R == 1) {
            }
            if (state_R == 3) {
            }
            state_R = 4;
        }
        
        
        
        if (wheel_L_A == 0 && wheel_L_B == 0) {
            if (state_L == 2) {
                state_L_prev = 2;
                Theta_l = Theta_l - 2*PIE/800;
                
                //ROS_INFO_STREAM(" LEFT WHEEL SPINNED BY ANGLE: " << Theta_l);
            }
            if (state_L == 4) {
                state_L_prev = 4;
                Theta_l = Theta_l + 2*PIE/800;
                //ROS_INFO_STREAM(" LEFT WHEEL SPINNED BY ANGLE: " << Theta_l);
            }
            state_L = 1;
        } 
        else if (wheel_L_A == 1 && wheel_L_B == 0) {
            if (state_L == 1) {
                state_L_prev = 1;
            }
            if (state_L == 3) {
                state_L_prev = 3;
            }
            state_L = 2;
        }
        else if (wheel_L_A == 1 && wheel_L_B == 1) {
            if (state_L == 2) {
                state_L_prev = 2;
            }
            if (state_L == 4) {
                state_L_prev = 4;
            }
            state_L = 3;
        }
        else {
            if (state_L == 1) {
                state_L_prev = 1;
            }
            if (state_L == 3) {
                state_L_prev = 3;
            }
            state_L = 4;
        }
        
        
        //ROS_INFO_STREAM("THIS IS ULAN");
		// Spin once so that this node can service any
		// callbacks that this node has queued.
// 		motor_ctrl msg = motor_ctrl();
// 		msg.theta_r_dot = Theta_r_dot;
// 		msg.theta_l_dot = Theta_l_dot;
// 		current_thetas.publish(msg);

		
		ros::spinOnce();

		// Sleep for the specified loop rate
		//loop_rate.sleep();
	} // END OF: "while (ros::ok())"


	return 0;
}
