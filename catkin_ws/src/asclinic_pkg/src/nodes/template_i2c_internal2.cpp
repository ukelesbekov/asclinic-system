#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/UInt16.h"
#include "std_msgs/Int32.h"

#include "i2c_driver/i2c_driver.h"

#include "pololu_smc_g2/pololu_smc_g2.h"

#include <bitset>
#include "asclinic_pkg/motor_ctrl.h"
#include "asclinic_pkg/error_msg.h"
using namespace asclinic_pkg;

// Member variables for this node
const char * m_i2c_device_name = "/dev/i2c-1";
I2C_Driver m_i2c_driver (m_i2c_device_name);

const uint8_t m_pololu_smc_address_left = 13;
Pololu_SMC_G2 m_pololu_smc_left (&m_i2c_driver, m_pololu_smc_address_left);

const uint8_t m_pololu_smc_address_right = 14;
Pololu_SMC_G2 m_pololu_smc_right (&m_i2c_driver, m_pololu_smc_address_right);

int current_task;

// Respond to subscriber receiving a message
// > To test this out without creating an additional
//   ROS node
//   1) First use the command:
//        rostopic list
//      To identify the full name of this subscription topic.
//   2) Then use the following command to send  message on
//      that topic
//        rostopic pub --once <namespace>/set_motor_duty_cycle std_msgs/UInt16 10
//      where "<namespace>/set_motor_duty_cycle" is the full
//      name identified in step 1.
// Uint16 before 

void SetPwmCallback(const motor_ctrl& msg)
{
	
    static int counter = 0;
    counter++;
    
	// Storing msg into local var
	float v = msg.v;
	//float omega = 3.14159/4;
	float omega = msg.omega;
	
	//ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] Message receieved with data, v: " << v << " omega: " << omega);
	
    float theta_l_dot = v/0.05-0.25/2*omega/0.05;
    float theta_r_dot = v/0.05+0.25/2*omega/0.05;
    ROS_INFO_STREAM("THETA_L_DOT: " << theta_l_dot << "THETA_R_DOT: " << theta_r_dot);
	// Initialise a pointer for iterating through
	// the Pololu SMC objects
	Pololu_SMC_G2 * pololu_smc_pointer;

	// Initialise one boolean variable for the result
	// of all calls to Pololu_SMC_G2 functions
	bool result1, result2;
    int temp1, temp2;
    float pwm1, pwm2;

	// Iterate over the pololu objects
	for (int i_smc=0;i_smc<2;i_smc++)
	{
		// Point to the appropriate motor controller
		if (i_smc==0) {
			pololu_smc_pointer = &m_pololu_smc_left;
			if (true)	{
			    //result1 = pololu_smc_pointer->set_motor_target_speed_percent(28+theta_dot_l_err);//.0373831776);//+ 5*theta_dot_l_err);//static_cast<int>
		        /* TODO: Map v and omega to PWM1 */
		        pwm1 = 0;
			 //   temp1 = static_cast<int>( (28.0 + pwm1) * 32.0 );
			 //   result1 = pololu_smc_pointer->set_motor_target_speed_3200(temp1);
			       //temp1 = static_cast<int>(theta_l_dot);//-28*10);//7*32);//theta_l_dot * 32.0 );
			       temp1 = static_cast<int>(2.4*theta_l_dot*32);//7*32);//theta_l_dot * 32.0 );
			       //ROS_INFO_STREAM("TEMP1 IS  " << temp1);
			       result1 = pololu_smc_pointer->set_motor_target_speed_3200(temp1);
			} else {
			    result1 = pololu_smc_pointer->set_motor_target_speed_percent(0);
			}
		} else {
			pololu_smc_pointer = &m_pololu_smc_right;
			if (true)	{
			    //result2 = pololu_smc_pointer->set_motor_target_speed_percent(-28.58-1.3*theta_dot_r_err);//-5*theta_dot_r_err);//28.7081339713);//28.5714285714);//-6*5.12);//-3.1415*5.1-theta_dot_r_err);
		        /* TODO: Map v and omega to PWM2 */
		        pwm2 = 0;
			 //   temp2 = static_cast<int>( (-28.58 - 1.6 * pwm2) * 32.0 );
			 //   result2 = pololu_smc_pointer->set_motor_target_speed_3200(temp2);
			    //temp2 = static_cast<int>(-theta_r_dot);//-28.58*10);//-7*32);//-theta_r_dot * 32.0 );
			    temp2 = static_cast<int>(-2.484*theta_r_dot*32);//-7*32);//-theta_r_dot * 32.0 )
			    //ROS_INFO_STREAM("TEMP2 IS  " << temp1);
			    result2 = pololu_smc_pointer->set_motor_target_speed_3200(temp2);
			} else {
			    result2 = pololu_smc_pointer->set_motor_target_speed_percent(0);
			}
		}
	}

	// Get the target speed value to check that
	// it was set correctly
    int16_t current_target_speed_value;
	// Iterate over the pololu objects
	for (int i_smc=0;i_smc<2;i_smc++)
	{
		// Point to the appropriate motor controller
		if (i_smc==0) {
			pololu_smc_pointer = &m_pololu_smc_left;
			result1 = pololu_smc_pointer->get_target_speed_3200(&current_target_speed_value);
			//ROS_INFO_STREAM("result 1 is " << result1);
			if (result1) {
			    //ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] Pololu SMC - get target speed value returned: " << current_target_speed_value << ", for I2C address " << pololu_smc_pointer->get_i2c_address() );
		    } else {
			    //ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] FAILED - Pololu SMC - get target speed value NOT successful for I2C address " << pololu_smc_pointer->get_i2c_address() );
		    }
		}
		else {
			pololu_smc_pointer = &m_pololu_smc_right;
			result2 = pololu_smc_pointer->get_target_speed_3200(&current_target_speed_value);
			//ROS_INFO_STREAM("result 2 is   " << result2);
            if (result2) {
			    //ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] Pololu SMC - get target speed value returned: " << current_target_speed_value << ", for I2C address " << pololu_smc_pointer->get_i2c_address() );
		    } else {
			    //ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] FAILED - Pololu SMC - get target speed value NOT successful for I2C address " << pololu_smc_pointer->get_i2c_address() );
		    }
		}
	}
}

void current_task_callback(const std_msgs::Int32 msg)
{
    current_task = msg.data;
}


// /* This is the old CallBack function using error_msg for Inner Loop Controller*/
// void templateSubscriberCallback(const error_msg& msg)
// {
// 	//ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] Message receieved with data = " << msg.theta_dot_r_err << " ___ " << msg.theta_dot_l_err);
//     static int counter = 0;
//     counter++;
// 	// Clip the data to be in the range [0,100]
// 	float theta_dot_r_err = msg.theta_dot_r_err;
// 	float theta_dot_l_err = msg.theta_dot_l_err;
// 	//int pwm_duty_cycle = 10*(1-msg.data);
// 	//if (pwm_duty_cycle < 0)
// 	//	pwm_duty_cycle = 0;
// 	//if (pwm_duty_cycle > 100)
// 	//	pwm_duty_cycle = 100;
    
// 	// Initialise a pointer for iterating through
// 	// the Pololu SMC objects
// 	Pololu_SMC_G2 * pololu_smc_pointer;

// 	// Initialise one boolean variable for the result
// 	// of all calls to Pololu_SMC_G2 functions
// 	bool result1, result2;
//     int temp1, temp2;
// 	// Set the target speed to be the same for
// 	// both motor controllers

// 	// Iterate over the pololu objects
// 	for (int i_smc=0;i_smc<2;i_smc++)
// 	{
// 		// Point to the appropriate motor controller
// 		if (i_smc==0) {
// 			pololu_smc_pointer = &m_pololu_smc_left;
// 			if (counter <=1000)
// 			{
// 			//result1 = pololu_smc_pointer->set_motor_target_speed_percent(28+theta_dot_l_err);//.0373831776);//+ 5*theta_dot_l_err);//static_cast<int>
// 			temp1 = static_cast<int>( (28.0 + theta_dot_l_err) * 32.0 );
// 			result1 = pololu_smc_pointer->set_motor_target_speed_3200(temp1);
// 			}
// 			else
// 			result1 = pololu_smc_pointer->set_motor_target_speed_percent(0);
// 		}
// 		//if (!result1)
// 		//ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] FAILED - Pololu SMC - set motor percent NOT successful for I2C address " << pololu_smc_pointer->get_i2c_address() );
// 		else {
// 			pololu_smc_pointer = &m_pololu_smc_right;
// 			if (counter <=1000)
// 			{
// 			//result2 = pololu_smc_pointer->set_motor_target_speed_percent(-28.58-1.3*theta_dot_r_err);//-5*theta_dot_r_err);//28.7081339713);//28.5714285714);//-6*5.12);//-3.1415*5.1-theta_dot_r_err);
// 			temp2 = static_cast<int>( (-28.58 - 1.6 * theta_dot_r_err) * 32.0 );
// 			result2 = pololu_smc_pointer->set_motor_target_speed_3200(temp2);
// 			}
// 			else
// 			result2 = pololu_smc_pointer->set_motor_target_speed_percent(0);
			
// 		}
// 			//result2 = pololu_smc_pointer->set_motor_target_speed_percent(0);
// 			//if (!result2)
// 			//ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] FAILED - Pololu SMC - set motor percent NOT successful for I2C address " << pololu_smc_pointer->get_i2c_address() );

// 		// Set the target speed
		
// 	}


// 	// Get the target speed value to check that
// 	// it was set correctly
//     int16_t current_target_speed_value;
// 	// Iterate over the pololu objects
// 	for (int i_smc=0;i_smc<2;i_smc++)
// 	{
// 		// Point to the appropriate motor controller
// 		if (i_smc==0) {
// 			pololu_smc_pointer = &m_pololu_smc_left;
// 			result1 = pololu_smc_pointer->get_target_speed_3200(&current_target_speed_value);
// 			//ROS_INFO_STREAM("result 1 is " << result1);
// 			if (result1)
// 		{
// 			//ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] Pololu SMC - get target speed value returned: " << current_target_speed_value << ", for I2C address " << pololu_smc_pointer->get_i2c_address() );
// 		}
// 		else
// 		{
// 			//ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] FAILED - Pololu SMC - get target speed value NOT successful for I2C address " << pololu_smc_pointer->get_i2c_address() );
// 		}
		
// 		}
// 		else
// 		{
// 			pololu_smc_pointer = &m_pololu_smc_right;
// 			result2 = pololu_smc_pointer->get_target_speed_3200(&current_target_speed_value);
// 			//ROS_INFO_STREAM("result 2 is   " << result2);
//             if (result2)
// 		{
// 			//ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] Pololu SMC - get target speed value returned: " << current_target_speed_value << ", for I2C address " << pololu_smc_pointer->get_i2c_address() );
// 		}
// 		else
// 		{
// 			//ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] FAILED - Pololu SMC - get target speed value NOT successful for I2C address " << pololu_smc_pointer->get_i2c_address() );
// 		}
// 		}
		
// 	}
// }



int main(int argc, char* argv[])
{
	// Initialise the node
	ros::init(argc, argv, "template_i2c_internal2");
	ros::NodeHandle nodeHandle("~");
	ros::NodeHandle GlobalNodeHandle("/global");
	// Initialise a publisher
	//ros::Publisher current_measurement_publisher = nodeHandle.advertise<std_msgs::UInt16>("current_measurement", 10, false);
	// Initialise a subscriber
	//ros::Subscriber set_motor_duty_cycle_subscriber = GlobalNodeHandle.subscribe("error_computation", 1, templateSubscriberCallback);
	ros::Subscriber motor_ctrl_subscriber = GlobalNodeHandle.subscribe("motor_ctrl", 1, SetPwmCallback);
	ros::Subscriber task = GlobalNodeHandle.subscribe("current_task", 1, current_task_callback);

	// Initialise a variable with loop rate for
	// polling the sensors
	// > Input argument is the frequency in hertz, as a double
	ros::Rate loop_rate(100);
	Pololu_SMC_G2 * p;
	p = &m_pololu_smc_left;
    p->set_motor_target_speed_percent(0);
    p = &m_pololu_smc_right;
    p->set_motor_target_speed_percent(0);
	
	// Open the I2C device
	// > Note that the I2C driver is already instantiated
	//   as a member variable of this node
	bool open_success = m_i2c_driver.open_i2c_device();
    
	// Display the status
	if (!open_success)
	{
		ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] FAILED to open I2C device named " << m_i2c_driver.get_device_name());
	}
	else
	{
		ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] Successfully opened named " << m_i2c_driver.get_device_name() << ", with file descriptor = " << m_i2c_driver.get_file_descriptor());
	}

	// Note:
	// > The drivers for each of the Pololu simple motor
	//   controllers (SMC) are already instantiated as
	//   member variables of this node

    

	// SET THE CONFIGURATION OF EACH MOTOR CONTROLLER

	// Specify the various limite
	int new_current_limit_in_milliamps = 5000;
	int new_max_speed_limit = 2560;
	int new_max_accel_limit = 1;
	int new_max_decel_limit = 5;

	// Initialise a pointer for iterating through
	// the Pololu SMC objects
	Pololu_SMC_G2 * pololu_smc_pointer;

	// Initialise one boolean variable for the result
	// of all calls to Pololu_SMC_G2 functions
	bool result;

	// Iterate over the pololu objects
	for (int i_smc=0;i_smc<2;i_smc++)
	{
		// Point to the appropriate motor controller
		if (i_smc==0)
			pololu_smc_pointer = &m_pololu_smc_left;
		else
			pololu_smc_pointer = &m_pololu_smc_right;

		ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] Now initialising SMC with I2C address " << pololu_smc_pointer->get_i2c_address() );

		// Send the "exit safe start" command
		result = pololu_smc_pointer->exit_safe_start();
		if (!result)
			ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] FAILED - Pololu SMC - exit safe start NOT successful for I2C address " << pololu_smc_pointer->get_i2c_address() );

		// > Check the status flag registers
		uint16_t error_status;
		result = pololu_smc_pointer->get_error_status(&error_status);
		if (result)
			ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] Pololu SMC - get error status returned: " << std::bitset<16>(error_status) << ", for I2C address " << pololu_smc_pointer->get_i2c_address() );
		else
			ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] FAILED - Pololu SMC - get error status NOT successful for I2C address " << pololu_smc_pointer->get_i2c_address() );

		// > Check the input voltage
		float input_voltage_value;
		result = pololu_smc_pointer->get_input_voltage_in_volts(&input_voltage_value);
		if (result)
			ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] Pololu SMC - get input voltage value returned: " << input_voltage_value << " [Volts], for I2C address " << pololu_smc_pointer->get_i2c_address() );
		else
			ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] FAILED - Pololu SMC - get input voltage value NOT successful for I2C address " << pololu_smc_pointer->get_i2c_address() );

		// Short sleep
		usleep(1000);


		// Set the current limit
		result = pololu_smc_pointer->set_current_limit_in_milliamps(new_current_limit_in_milliamps);
		if (!result)
			ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] FAILED - Pololu SMC - set current limit NOT successful for I2C address " << pololu_smc_pointer->get_i2c_address() );

		// Check the current limit that was set
		uint16_t current_limit_value;
		result = pololu_smc_pointer->get_current_limit(&current_limit_value);
		if (result)
			ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] Pololu SMC - get current limit returned: " << current_limit_value << ", for I2C address " << pololu_smc_pointer->get_i2c_address() );
		else
			ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] FAILED - Pololu SMC - get current limit NOT successful for I2C address " << pololu_smc_pointer->get_i2c_address() );

		// Short sleep
		usleep(1000);

		// Send the max speed limit
		int max_speed_limit_response_code;
		result = pololu_smc_pointer->set_motor_limit_max_speed(new_max_speed_limit, &max_speed_limit_response_code);
		if (!result)
			ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] FAILED - Pololu SMC - set max speed limit NOT successful with response code " << max_speed_limit_response_code << ", for I2C address " << pololu_smc_pointer->get_i2c_address() );

		// Check the max speed limit that was set
		uint16_t max_speed_limit_value;
		result = pololu_smc_pointer->get_max_speed_forward(&max_speed_limit_value);
		if (result)
			ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] Pololu SMC - get max speed limit returned: " << max_speed_limit_value << ", for I2C address " << pololu_smc_pointer->get_i2c_address() );
		else
			ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] FAILED - Pololu SMC - get max speed limit NOT successful for I2C address " << pololu_smc_pointer->get_i2c_address() );

		// Short sleep
		usleep(1000);

		// Set the max acceleration limit
		int max_accel_limit_response_code;
		result = pololu_smc_pointer->set_motor_limit_max_acceleration(new_max_accel_limit, &max_accel_limit_response_code);
		if (!result)
			ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] FAILED - Pololu SMC - set max acceleration limit NOT successful with response code " << max_accel_limit_response_code << " for I2C address " << pololu_smc_pointer->get_i2c_address() );

		// Check the max speed acceleration that was set
		uint16_t max_accel_limit_value;
		result = pololu_smc_pointer->get_max_acceleration_forward(&max_accel_limit_value);
		if (result)
			ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] Pololu SMC - get max acceleration limit returned: " << max_accel_limit_value << ", for I2C address " << pololu_smc_pointer->get_i2c_address() );
		else
			ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] FAILED - Pololu SMC - get max acceleration limit NOT successful for I2C address " << pololu_smc_pointer->get_i2c_address() );

		// Set the max deceleration limit
		int max_decel_limit_response_code;
		result = pololu_smc_pointer->set_motor_limit_max_deceleration(new_max_decel_limit, &max_decel_limit_response_code);
		if (!result)
			ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] FAILED - Pololu SMC - set max deceleration limit NOT successful with response code " << max_decel_limit_response_code << " for I2C address " << pololu_smc_pointer->get_i2c_address() );

		// > Check the max speed deceleration that was set
		uint16_t max_decel_limit_value;
		result = pololu_smc_pointer->get_max_deceleration_forward(&max_decel_limit_value);
		if (result)
			ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] Pololu SMC - get max deceleration limit returned: " << max_decel_limit_value << ", for I2C address " << pololu_smc_pointer->get_i2c_address() );
		else
			ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] FAILED - Pololu SMC - get max deceleration limit NOT successful for I2C address " << pololu_smc_pointer->get_i2c_address() );

		// Short sleep
		usleep(1000);

		ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] Finished setting up the Pololu SMC with I2C address " << pololu_smc_pointer->get_i2c_address() );
	}



	// Enter a loop that continues while ROS is still running
	while (ros::ok())
	{
		// Reading of the current sensor to be implemented here

        //ROS_INFO_STREAM("THIS IS ULAN");
		// Spin once so that this node can service any
		// callbacks that this node has queued.
		ros::spinOnce();
        
		// Sleep for the specified loop rate
		loop_rate.sleep();
	} // END OF: "while (ros::ok())"

	// Close the I2C device
	bool close_success = m_i2c_driver.close_i2c_device();
	
	// Display the status
	if (!close_success)
	{
		ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] FAILED to close I2C device named " << m_i2c_driver.get_device_name());
	}
	else
	{
		ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] Successfully closed device named " << m_i2c_driver.get_device_name());
	}

	return 0;
}
