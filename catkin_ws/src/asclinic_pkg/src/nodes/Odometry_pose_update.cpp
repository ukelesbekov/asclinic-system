#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/UInt16.h"
#include "std_msgs/Int32.h"
#include <cmath>
#include "i2c_driver/i2c_driver.h"

#include "pololu_smc_g2/pololu_smc_g2.h"

#include <bitset>
#include "asclinic_pkg/encoder_msg2.h"
#include "asclinic_pkg/motor_ctrl.h"
#include "asclinic_pkg/pose_msg.h"
using namespace asclinic_pkg;

#define PIE atan(1)*4

int x[4] = {0,0,0,0};
float pose[3] = {0,0,PIE/2};

float Theta_r = 0, Theta_l = 0;


ros::Timer timer_for_odometry;
ros::Timer timer_for_motor_ctrl;
ros::Publisher pose_publisher;




void timerCallbackForOdometry(const ros::TimerEvent&)
{
	// Declare a static integer for publishing a different
	// number each time
	static uint counter = 0;

	// Increment the counter
	counter++;
    float delta_s = 0.05/2*(Theta_l+Theta_r);
    float delta_phi = 0.05/0.25*(Theta_r-Theta_l);
    
    pose[0] = pose[0] + delta_s*cos(pose[2]+1/2*delta_phi);
    pose[1] = pose[1] + delta_s*sin(pose[2]+1/2*delta_phi);
    pose[2] = pose[2] + delta_phi;
    // if (counter%100 == 0) {
    //     ROS_INFO_STREAM("POSE x: " << pose[0] << "        y: " << pose[1] << "     phi: " << pose[2]);
    // }
    Theta_l = 0;
    Theta_r = 0;
    
    ROS_INFO_STREAM("CURRENT POSE IS:   x_r: " << pose[0] << "     y_r: " << pose[1] << "     phi: " << pose[2]);
    pose_msg pose_message = pose_msg();
    pose_message.x_p = pose[0];
    pose_message.y_p = pose[1];
    pose_message.phi = pose[2];
    pose_publisher.publish(pose_message);
	// START THE TIMER AGAIN
	// > Stop any previous instance that might still be running
	timer_for_odometry.stop();
	// > Set the period again (second argument is reset)
	timer_for_odometry.setPeriod( ros::Duration(0.01), true);
	// > Start the timer again
	timer_for_odometry.start();
}



void templateSubscriberCallback1(const encoder_msg2& msg)
{
	
	int recA = msg.eventA;
	int recB = msg.eventB;
	x[0] = recA;
	x[1] = recB;
	if (x[0] == 1 && x[1] == 0) {
	    Theta_r = Theta_r - 2*PIE/800;
	}
	else if (x[0] == 1 && x[1] == 1){
	    Theta_r = Theta_r + 2*PIE/800;
	}
	//ROS_INFO_STREAM("WHEEL RIGHT, message received: edgeA is " << recA << "  and edgeB is " << recB);
}

void templateSubscriberCallback2(const encoder_msg2& msg)
{
	
    int recA = msg.eventA;
	int recB = msg.eventB;
	x[2] = recA;
	x[3] = recB;
	if (x[2] == 1 && x[3] == 0) {
	    Theta_l = Theta_l + 2*PIE/800;
	}
	else if (x[2] == 1 && x[3] == 1){
	    Theta_l = Theta_l - 2*PIE/800;
	}
	//ROS_INFO_STREAM("WHEEL LEFT, message received: edgeA is " << recA << "  and edgeB is " << recB);
}



int main(int argc, char* argv[])
{
	// Initialise the node
	ros::init(argc, argv, "Odometry");
	ros::NodeHandle nodeHandle("~");
	ros::NodeHandle GlobalNodeHandle("/global");
	ros::NodeHandle NH1("/eventR");
	ros::NodeHandle NH2("/eventL");
	
	// Initialise a publisher

	pose_publisher = GlobalNodeHandle.advertise<pose_msg>("current_pose",10,false);
	// Initialise a subscriber
	ros::Subscriber set_motor_duty_cycle_subscriber1 = NH1.subscribe("gpio_event_R", 100, templateSubscriberCallback1);
	ros::Subscriber set_motor_duty_cycle_subscriber2 = NH2.subscribe("gpio_event_L", 100, templateSubscriberCallback2);
	

    timer_for_odometry = nodeHandle.createTimer(ros::Duration(0.01), timerCallbackForOdometry, true);
    
	//ros::Rate loop_rate(50);
	
	
	ros::spin();

	return 0;
}
