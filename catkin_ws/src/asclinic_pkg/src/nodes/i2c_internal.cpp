// Copyright (C) 2021, The University of Melbourne, Department of Electrical and Electronic Engineering (EEE)
//
// This file is part of ASClinic-System.
//    
// See the root of the repository for license details.
//
// ----------------------------------------------------------------------------
//     _    ____   ____ _ _       _          ____            _                 
//    / \  / ___| / ___| (_)____ (_) ___    / ___| _   _ ___| |_ ___ ________  
//   / _ \ \___ \| |   | | |  _ \| |/ __|___\___ \| | | / __| __/ _ \  _   _ \ 
//  / ___ \ ___) | |___| | | | | | | (_|_____|__) | |_| \__ \ ||  __/ | | | | |
// /_/   \_\____/ \____|_|_|_| |_|_|\___|   |____/ \__, |___/\__\___|_| |_| |_|
//                                                 |___/                       
//
// DESCRIPTION:
// Template node for I2C devices connected inside the robot
//
// ----------------------------------------------------------------------------





#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/UInt16.h"
#include "std_msgs/Int32.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Bool.h"
#include <math.h>
#include "i2c_driver/i2c_driver.h"

#include "pololu_smc_g2/pololu_smc_g2.h"

#include "pca9685/pca9685.h"

#include <bitset>

// Include the asclinic message types
#include "asclinic_pkg/ServoPulseWidth.h"

// Namespacing the package
using namespace asclinic_pkg;

// Local variable for current task ID
int current_task;


// MEMBER VARIABLES FOR THIS NODE:
// > For the I2C driver
const char * m_i2c_device_name = "/dev/i2c-1";
I2C_Driver m_i2c_driver (m_i2c_device_name);

// > For the PCA9685 PWM Servo Driver driver
const uint8_t m_pca9685_address = 0x42;
PCA9685 m_pca9685_servo_driver (&m_i2c_driver, m_pca9685_address);


bool done_deliver_flag = false;

void templateServoSubscriberCallback(const ServoPulseWidth& msg)
{
	// Extract the channel and pulse width from the message
	uint8_t channel = msg.channel;
	uint16_t pulse_width_in_us = msg.pulse_width_in_microseconds;

	// Display the message received
	ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] Message receieved for servo with channel = " << static_cast<int>(channel) << ", and pulse width [us] = " << static_cast<int>(pulse_width_in_us) );

	// Limit the pulse width to be either:
	// > zero
	// > in the range [1000,2000]
// 	if (pulse_width_in_us > 0)
// 	{
// 		if (pulse_width_in_us < 1000)
// 			pulse_width_in_us = 1000;
// 		if (pulse_width_in_us > 2000)
// 			pulse_width_in_us = 2000;
// 	}

	// Call the function to set the desired pulse width
	//First open the door
    bool result = m_pca9685_servo_driver.set_pwm_pulse_in_microseconds(channel, 1700+pulse_width_in_us);
	// Display if an error occurred
	if (!result)
	{
		ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] FAILED to set pulse width for servo at channel " << static_cast<int>(channel) );
	}
	
	//Delay for 1 sec
	ros::Duration(2.0).sleep();
	
	//To turn in opposite direction
	//m_pca9685_servo_driver.set_output_logic_invert_mode(true);
// 	result = m_pca9685_servo_driver.set_pwm_pulse_in_microseconds(channel, pulse_width_in_us-700);
	ROS_INFO_STREAM("I'M HERE TO REVERSE" );
	//Now close the door
	result = m_pca9685_servo_driver.set_pwm_pulse_in_microseconds(channel, 900);
	// Display if an error occurred
	if (!result)
	{
		ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] FAILED to set pulse width for servo at channel " << static_cast<int>(channel) );
	}
	
	done_deliver_flag = true;

}


// Callback function for reading/updating the current task ID
void current_task_callback(const std_msgs::Int32 msg)
{
    current_task = msg.data;
}

int main(int argc, char* argv[])
{
	// Initialise the node
	ros::init(argc, argv, "template_i2c_internal");
	ros::NodeHandle nodeHandle("~");
	ros::NodeHandle GlobalNodeHandle("/global");
	// Initialise a publisher
	ros::Publisher set_servo_pulse_width_publisher = GlobalNodeHandle.advertise<ServoPulseWidth>("set_servo_pulse_width", 1, false);
    
	// Initialise a subscriber for the servo driver
	ros::Subscriber set_servo_pulse_width_subscriber = GlobalNodeHandle.subscribe("set_servo_pulse_width", 1, templateServoSubscriberCallback);
	
	// Initialise a subscriber for the current task
	ros::Subscriber task = GlobalNodeHandle.subscribe("current_task", 1, current_task_callback);
	
	ros::Publisher task_8_done = GlobalNodeHandle.advertise<std_msgs::Bool>("task_8_done", 10, false);

	// Open the I2C device
	// > Note that the I2C driver is already instantiated
	//   as a member variable of this node
	bool open_success = m_i2c_driver.open_i2c_device();

	// Display the status
	if (!open_success)
	{
		ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] FAILED to open I2C device named " << m_i2c_driver.get_device_name());
	}
	else
	{
		ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] Successfully opened named " << m_i2c_driver.get_device_name() << ", with file descriptor = " << m_i2c_driver.get_file_descriptor());
	}

	// Note:
	// > The drivers are already instantiated as
	//   member variables of this node for:
	//   > Each of the Pololu simple motor controllers (SMC)
	//   > The servo driver


	// SET THE CONFIGURATION OF THE PERVO DRIVER

	// Specify the frequency of the servo driver
	float new_frequency_in_hz = 50.0;

	// Call the Servo Driver initialisation function
	bool verbose_display_for_servo_driver_init = false;
	bool result_servo_init = m_pca9685_servo_driver.initialise_with_frequency_in_hz(new_frequency_in_hz, verbose_display_for_servo_driver_init);

	// Display if an error occurred
	if (!result_servo_init)
	{
		ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] FAILED - while initialising servo driver with I2C address " << static_cast<int>(m_pca9685_servo_driver.get_i2c_address()) );
	}

    while (ros::ok()){
        if (current_task == 8){
            ServoPulseWidth msg = ServoPulseWidth();
		    msg.channel = 14;
		    msg.pulse_width_in_microseconds = 500; //2000;
		    set_servo_pulse_width_publisher.publish(msg);

        }
        if (done_deliver_flag == true){
            //Then, update Task8 done
            std_msgs::Bool task8;
            task8.data = true;
            task_8_done.publish(task8); 
            done_deliver_flag = false;
        }

    	ros::spinOnce();
    }
	// Close the I2C device
	bool close_success = m_i2c_driver.close_i2c_device();
	
	// Display the status
	if (!close_success)
	{
		ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] FAILED to close I2C device named " << m_i2c_driver.get_device_name());
	}
	else
	{
		ROS_INFO_STREAM("[TEMPLATE I2C INTERNAL] Successfully closed device named " << m_i2c_driver.get_device_name());
	}

	return 0;
}
