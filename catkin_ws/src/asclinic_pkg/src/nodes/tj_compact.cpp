// Copyright (C) 2021, The University of Melbourne, Department of Electrical and Electronic Engineering (EEE)
//
// This file is part of ASClinic-System.
//
// See the root of the repository for license details.
//
// ----------------------------------------------------------------------------
//     _    ____   ____ _ _       _          ____            _
//    / \  / ___| / ___| (_)____ (_) ___    / ___| _   _ ___| |_ ___ ________
//   / _ \ \___ \| |   | | |  _ \| |/ __|___\___ \| | | / __| __/ _ \  _   _ \
//  / ___ \ ___) | |___| | | | | | | (_|_____|__) | |_| \__ \ ||  __/ | | | | |
// /_/   \_\____/ \____|_|_|_| |_|_|\___|   |____/ \__, |___/\__\___|_| |_| |_|
//                                                 |___/
//
// DESCRIPTION:
// Template node for polling the value of a GPIO pin at a fixed frequency
//
// ----------------------------------------------------------------------------





#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/Int32.h"
#include "std_msgs/Bool.h"
#include <math.h>
#include <gpiod.h>
#include "asclinic_pkg/motor_ctrl.h"
#include "asclinic_pkg/error_msg.h"
#include "asclinic_pkg/pose_msg.h"

#define PI 3.14159
using namespace asclinic_pkg;

ros::Timer timer_debug;
ros::Timer timer_for_pose_ref_pub;
ros::Publisher pose_ref_publisher;

// Current pose - local to this node
float current_pose[3];

// Reference to be posted and tracked - local to this node
float pose_ref[3];

// Compact trajectory storage
float x_p[3];
float y_p[3];
float phi[3];

// Variable that detects whether we need to turn at the first stage
bool need_to_turn_first = false;

// time variable for trajectory generation
float pose_time = 0;
int tim = 0;

// Base angular velocity for turning
float omega_base = 1; // 1 radian per second to be achieved

// Base speed for driving
float v_base = 0.23;

// Buffer variables for storing values temporarily
float x_buff, y_buff, phi_buff;

// Local variable for current task ID
int current_task;


// Callback function for reading the current pose
void read_the_pose(const pose_msg& msg)
{
    
    current_pose[0] = msg.x_p;
    current_pose[1] = msg.y_p;
    current_pose[2] = msg.phi;
    //ROS_INFO_STREAM("POSE RECEIVED: " << current_pose[0] << current_pose[1] << current_pose[2]);
}

// Callback function for reading/updating the current task ID
void current_task_callback(const std_msgs::Int32 msg)
{
    current_task = msg.data;
}

// Publish reference pose
void timerCallbackForPublishingReferencePose(const ros::TimerEvent&)
{
	
	static uint counter = 0;

	counter++;
	
	// helps generate trajectory (every 10ms publishes next trajectory)
    pose_time += 0.01;
    // Decide what is the current reference to be tracked
    pose_msg msg = pose_msg();
    msg.x_p = pose_ref[0];
	msg.y_p = pose_ref[1];
	msg.phi = pose_ref[2];
	
	//ROS_INFO_STREAM("WE ARE PUBLISHING REF: "<< msg.x_p << "   " << msg.y_p << "   " << msg.phi);
    pose_ref_publisher.publish(msg);
    
	// START THE TIMER AGAIN
	// > Stop any previous instance that might still be running
	timer_for_pose_ref_pub.stop();
	// > Set the period again (second argument is reset)
	//ROS_INFO_STREAM("===========================");
	ROS_INFO_STREAM("TIME: " << pose_time << "     TASK: " << current_task << "     REF: x_ref=" << msg.x_p << "   y_ref="  << msg.y_p << "   phi_ref=" << msg.phi);
	ROS_INFO_STREAM("POSE: x_p=" << current_pose[0] << "  y_p="  << current_pose[1]<< "   phi=" << current_pose[2]);
	//ROS_INFO_STREAM("===========================");
	timer_for_pose_ref_pub.setPeriod( ros::Duration(0.01), true);
	// > Start the timer again
	timer_for_pose_ref_pub.start();
	
}

// Timer callback for debugging: prints something useful every second
void timerCallback(const ros::TimerEvent&)
{
	
	static uint counter = 0;

	counter++;
    tim++;
	timer_debug.stop();
	// > Set the period again (second argument is reset)
	timer_debug.setPeriod( ros::Duration(1), true);
	// > Start the timer again
	timer_debug.start();
	//ROS_INFO_STREAM("TIME IS: " << counter << "SEC;   CURRENT TASK: " << current_task);
}



int main(int argc, char* argv[])
{
	// Initialise the node
	ros::init(argc, argv, "tj_compact");
	// Define 2 nodehandles
	ros::NodeHandle nodeHandle("~");
	ros::NodeHandle GlobalNodeHandle("/global");
	// Initialise a publisher for reference pose
	pose_ref_publisher = GlobalNodeHandle.advertise<pose_msg>("pose_reference", 10, false);
	
	// Initialise publishers for when certain task is done
	ros::Publisher task_1_done = GlobalNodeHandle.advertise<std_msgs::Bool>("task_1_done", 10, false);
	ros::Publisher task_2_done = GlobalNodeHandle.advertise<std_msgs::Bool>("task_2_done", 10, false);
	ros::Publisher task_3_done = GlobalNodeHandle.advertise<std_msgs::Bool>("task_3_done", 10, false);
	ros::Publisher task_4_done = GlobalNodeHandle.advertise<std_msgs::Bool>("task_4_done", 10, false);
	ros::Publisher task_5_done = GlobalNodeHandle.advertise<std_msgs::Bool>("task_5_done", 10, false);
	ros::Publisher task_6_done = GlobalNodeHandle.advertise<std_msgs::Bool>("task_6_done", 10, false);
	ros::Publisher task_7_done = GlobalNodeHandle.advertise<std_msgs::Bool>("task_7_done", 10, false);
	// Initialise subscriber to current pose
	ros::Subscriber pose_reading = GlobalNodeHandle.subscribe("current_pose", 1, read_the_pose);
	
	// Initialise subscriber to current task
	ros::Subscriber task = GlobalNodeHandle.subscribe("current_task", 1, current_task_callback);
	
	// Timer that sets period between reference pose publications
    timer_for_pose_ref_pub = nodeHandle.createTimer(ros::Duration(0.01), timerCallbackForPublishingReferencePose, true);
    
    // Timer that publishes something useful for debugging purposes
    timer_debug = nodeHandle.createTimer(ros::Duration(1), timerCallback, true);
    
    
    
	//ros::Rate loop_rate(100);
	//ros::spin();
 	while (ros::ok())
 	{
 	    // Depending on the task, generates trajectory
 	    switch (current_task) {
 	        
 	        // If current task is go to receptacle at [0, 1.6315, 90degrees]
 	        case 1:
 	        //ROS_INFO_STREAM("CT 1: " << current_task);
 	        {
 	        pose_ref[0] = current_pose[0];
 	        pose_ref[1] = current_pose[1];
 	        pose_ref[2] = current_pose[2];
 	        
 	        // If we want to generate trajectory
            x_p[0] = current_pose[0];
            y_p[0] = current_pose[1];
            phi[0] = current_pose[2];
            
            // CHECK IF WE ARE OFF Y-AXIS OR NOT
            if (false){//)x_p[0] != 0) {
                phi_buff = atan2((1.6315-y_p[0]), (0-x_p[0]));
            }
            else {
                phi_buff = PI/2;
            }
                // CHANGE TO RANGE OF VALUES
                if (false){// abs(phi[0] - phi_buff) >= 0.1) {
                    need_to_turn_first = true;
                    x_p[1] = current_pose[0];
                    y_p[1] = current_pose[1];
                    phi[1] = phi_buff;
                    
                    x_p[2] = 0;
                    y_p[2] = 1.6315;
                    phi[2] = phi_buff;
                }
                else {
                    need_to_turn_first = false;
                    x_p[1] = 0;
                    y_p[1] = 1.6315;
                    phi[1] = phi_buff;
                    
                    x_p[2] = 0;
                    y_p[2] = 1.6315;
                    phi[2] = phi_buff;
                }
                std_msgs::Bool task1;
                task1.data = true;
                task_1_done.publish(task1);
                pose_time = 0;
                break;
 	        }
            case 2:
            //ROS_INFO_STREAM("CT 2: " << current_task);
            {
                if (need_to_turn_first) {
                    if (abs(current_pose[2]-phi_buff) > 0.15) {
                        pose_ref[0] = x_p[0];
                        pose_ref[1] = y_p[0];
                            if (phi[0]+pose_time*omega_base > phi_buff) {
                                pose_ref[2] = phi_buff;
                            }
                            else {
                                pose_ref[2] = phi[0]+pose_time*omega_base;
                            }
                        
                        //ROS_INFO_STREAM("TIME IS: " << pose_time);
                    }
                    else {
                        pose_time = 0;
                        //need_to_turn_first = false;
                        std_msgs::Bool task2;
                        task2.data = true;
                    task_2_done.publish(task2);
                    }
                }
                else {
                    pose_time = 0;
                    std_msgs::Bool task2;
                    task2.data = true;
                    task_2_done.publish(task2);
                }
                break;
            }
            
            case 3:
            //ROS_INFO_STREAM("CT 3: " << current_task);
            {
                    if (y_p[0]+pose_time*v_base*sin(phi[2]) <= 1.6315) {
                        x_buff = 0;// x_p[0];//+pose_time*v_base*cos(phi[2]);
                        pose_ref[0] = x_buff;
                        y_buff = y_p[0]+pose_time*v_base*sin(phi[2]);
                        pose_ref[1] = y_buff;
                        
                        pose_ref[2] = phi[2];
                        
                        
                    }
                    else {
                        pose_ref[0] = 0;//x_buff;
                        pose_ref[1] = 1.6315;//y_buff;
                        pose_ref[2] = phi[2];
                    }
                    
                
                // If we reached the final destination then go to task 3
                if ( sqrt(pow((current_pose[0]-x_p[2]),2) + pow((current_pose[1]-y_p[2]), 2)) <= 0.08) { ///// CHANGE TO 0.1////////////////////////
                    pose_time = 0;
                    std_msgs::Bool task3;
                    task3.data = true;
                   task_3_done.publish(task3);
                }
                break;
            }
            case 4: // SERVO WORKING HERE
            {   
                pose_ref[0] = current_pose[0];
 	            pose_ref[1] = current_pose[1];
 	            pose_ref[2] = current_pose[2];
 	            ros::Duration(3.0).sleep();
                ROS_INFO_STREAM("STANDBY ON TASK 4");
                std_msgs::Bool task4;
                task4.data = true;
                task_4_done.publish(task4);
                pose_time = 0;
                
                    
                break;
            }
             case 5:
             {
                //ROS_INFO_STREAM("CT 5: " << current_task);
                
                x_p[0] = current_pose[0];
                y_p[0] = current_pose[1];
                phi[0] = current_pose[2];
            
            
                phi_buff = PI/2;
                
                
            
                    
                    x_p[1] = 0;
                    y_p[1] = -1.6315+0.05;
                    phi[1] = phi_buff;
                    
                    x_p[2] = 0;
                    y_p[2] = -1.6315+0.05;
                    phi[2] = phi_buff;
                std_msgs::Bool task5;
                task5.data = true;
                task_5_done.publish(task5);
                pose_time = 0;
                break;
                
 	    }   
              case 6:
              {
                    if (y_p[0]-pose_time*v_base*sin(phi[2]) >= -1.6315+0.05) {
                        x_buff = 0;// x_p[0];//+pose_time*v_base*cos(phi[2]);
                        pose_ref[0] = x_buff;
                        y_buff = y_p[0]-pose_time*v_base;//*sin(phi[2]);
                        pose_ref[1] = y_buff;
                        
                        pose_ref[2] = phi[2];
                        
                        
                    }
                    else {
                        pose_ref[0] = 0;//x_buff;
                        pose_ref[1] = -1.6315+0.05;//y_buff;
                        pose_ref[2] = phi[2];
                    }
                    
                
                // If we reached the final destination then go to task 3
                if ( sqrt(pow((current_pose[0]-x_p[2]),2) + pow((current_pose[1]-y_p[2]), 2)) <= 0.1) {
                    pose_time = 0;
                    std_msgs::Bool task6;
                    task6.data = true;
                   task_6_done.publish(task6);
                }
                break;
            }
                
            case 7:
            { //currently task 7
            // pose_ref[0] = 0;
 	          //  pose_ref[1] = -1.6315;
 	          //  pose_ref[2] = PI/2;
 	          //  if (abs(current_pose[2] - pose_ref[2]) <= 0.1) {
 	                std_msgs::Bool task7;
                    task7.data = true;
                   task_7_done.publish(task7);
 	          //  };
 	            ROS_INFO_STREAM("STANDBY ON TASK 7");
                break;
            
            }
            case 8:
            {
                pose_ref[0] = current_pose[0];
                pose_ref[1] = current_pose[1];
                pose_ref[2] = current_pose[2];
                ROS_INFO_STREAM("STANDBY ON TASK 8");
            }
            // case 9:
            // {
            //     pose_ref[0] = current_pose[0];
            //     pose_ref[1] = current_pose[1];
            //     pose_ref[2] = current_pose[2];
            //     ROS_INFO_STREAM("STANDBY ON TASK 9");
            // }
            
        }
    ros::spinOnce();
    }
    return 0;
}
