// Copyright (C) 2021, The University of Melbourne, Department of Electrical and Electronic Engineering (EEE)
//
// This file is part of ASClinic-System.
//    
// See the root of the repository for license details.
//
// ----------------------------------------------------------------------------
//     _    ____   ____ _ _       _          ____            _                 
//    / \  / ___| / ___| (_)____ (_) ___    / ___| _   _ ___| |_ ___ ________  
//   / _ \ \___ \| |   | | |  _ \| |/ __|___\___ \| | | / __| __/ _ \  _   _ \ 
//  / ___ \ ___) | |___| | | | | | | (_|_____|__) | |_| \__ \ ||  __/ | | | | |
// /_/   \_\____/ \____|_|_|_| |_|_|\___|   |____/ \__, |___/\__\___|_| |_| |_|
//                                                 |___/                       
//
// DESCRIPTION:
// Template node for mointoring edge events on a GPIO pin
//
// ----------------------------------------------------------------------------





#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/Int32.h"

#include <gpiod.h>
#include "asclinic_pkg/encoder_msg2.h"

using namespace asclinic_pkg;



int main(int argc, char* argv[])
{
	// Initialise the node
	ros::init(argc, argv, "wheel_left_event_triggered");
	ros::NodeHandle nodeHandle("~");
	ros::NodeHandle NH("/eventL");
	// Initialise a publisher
	ros::Publisher gpio_event_publisher = NH.advertise<encoder_msg2>("gpio_event_L", 100, false);
	
	const char * gpio_chip_name = "/dev/gpiochip0";

	int line_number_L_A = 105;
	int line_number_L_B = 160;

	ROS_INFO_STREAM("[TEMPLATE GPIO EVENT TRIG.] Will monitor \"line_number\" = " << line_number_L_A);

	// Initialise a GPIO chip, line, and event object
	struct gpiod_chip *chip;
	struct gpiod_line *line_L_A;
	struct gpiod_line *line_L_B;
	struct gpiod_line_event eventA;
	int eventB;

	// Specify the timeout specifications
	// > The first entry is seconds
	// > The second entry is nano-seconds
	struct timespec ts = { 0, 100000000 };
	
	// Intialise a variable for the flags returned
	// by GPIO calls
	int returned_wait_flag;
	int returned_read_flag;

	// Initialise variables used for computing the time
	// between events on the GPIO line
	time_t prev_tv_sec    = -1;
	long int prev_tv_nsec = -1;
	bool prev_time_isValid = false;

	// Get and print the value of the GPIO line
	// > Note: the third argument to "gpiod_ctxless_get_value"
	//   is an "active_low" boolean input argument.
	//   If true, this indicate to the function that active state
	//   of this line is low.
	int value_L_A, value_L_B;
	value_L_A = gpiod_ctxless_get_value(gpio_chip_name, line_number_L_A, true, "foobar");
	value_L_B = gpiod_ctxless_get_value(gpio_chip_name, line_number_L_B, true, "foobar");
	ROS_INFO_STREAM("[TEMPLATE GPIO EVENT TRIG.] On startup of node, chip " << gpio_chip_name << " line " << line_number_L_A << " returned value = " << value_L_A);
	ROS_INFO_STREAM("[TEMPLATE GPIO EVENT TRIG.] On startup of node, chip " << gpio_chip_name << " line " << line_number_L_B << " returned value = " << value_L_B);

	// Open the GPIO chip
	chip = gpiod_chip_open(gpio_chip_name);
	// Retrieve the GPIO line
	line_L_A = gpiod_chip_get_line(chip,line_number_L_A);
	// Display the status
	ROS_INFO_STREAM("[TEMPLATE GPIO EVENT TRIG.] Chip " << gpio_chip_name << " opened and line " << line_number_L_A << " retrieved");
	
	line_L_B = gpiod_chip_get_line(chip,line_number_L_B);
	// Display the status
	ROS_INFO_STREAM("[TEMPLATE GPIO EVENT TRIG.] Chip " << gpio_chip_name << " opened and line " << line_number_L_B << " retrieved");
	
	

	// Request the line events to be mointored
	// > Note: only one of these should be uncommented
	gpiod_line_request_rising_edge_events(line_L_A, "foobar");
	//gpiod_line_request_falling_edge_events(line, "foobar");
	//gpiod_line_request_both_edges_events(line, "foobar");

	// Display the line event values for rising and falling
	ROS_INFO_STREAM("[TEMPLATE GPIO EVENT TRIG.] The constants defined for distinguishing line events are:, GPIOD_LINE_EVENT_RISING_EDGE = " << GPIOD_LINE_EVENT_RISING_EDGE << ", and GPIOD_LINE_EVENT_FALLING_EDGE = " << GPIOD_LINE_EVENT_FALLING_EDGE);

	// Enter a loop that continues while ROS is still running
	
	while (ros::ok())
	{

		// Monitor for the requested events on the GPIO line
		// > Note: the function "gpiod_line_event_wait" returns:
		//    0  if wait timed out
		//   -1  if an error occurred
		//    1  if an event occurred.
		returned_wait_flag = gpiod_line_event_wait(line_L_A,&ts);

		// Respond based on the the return flag
		switch (returned_wait_flag)
		{
			// Event occurred:
			case 1:
			{
				// Read the pending event on the GPIO line
				// > Note: the function "gpiod_line_event_read" returns:
				//    0  if the event was read correctly
				//   -1  if an error occurred
				returned_read_flag = gpiod_line_event_read(line_L_A,&eventA);

				switch (returned_read_flag)
				{
					// Event read correctly
					case 0:
					{
						if (prev_time_isValid)
						{
						    eventB = gpiod_ctxless_get_value(gpio_chip_name, line_number_L_B, false, "foobar");
							// Compute the time difference in seconds
							float this_diff_sec = float(eventA.ts.tv_nsec - prev_tv_nsec) / 1000000000;
							if (eventA.ts.tv_sec > prev_tv_sec)
							{
								this_diff_sec = this_diff_sec + (eventA.ts.tv_sec - prev_tv_sec);
							}

							// Display the event
							//ROS_INFO_STREAM("event type = " << event.event_type << ", time delta (sec) = " << this_diff_sec);

							// Publish a message
							encoder_msg2 msg = encoder_msg2();
							msg.eventA = 1;
							//std_msgs::Int32 msg;
							msg.time_of_event = this_diff_sec;
							msg.eventB = eventB;
							gpio_event_publisher.publish(msg);
                
                            
                            //ROS_INFO_STREAM("COUNTS: " << x << "    TIME: " << tim);
							// Spin once so that this node can service the any
							// callbacks that this node has queued.
							// > This is required so that message is actually
							//   published.
							ros::spinOnce();
						}

						// Update the previous time
						prev_tv_sec  = eventA.ts.tv_sec;
						prev_tv_nsec = eventA.ts.tv_nsec;
						prev_time_isValid = true;
						break;
					}

					// Error occurred
					case -1:
					{
						// Display the status
						ROS_INFO("[TEMPLATE GPIO EVENT TRIG.] gpiod_line_event_wait returned the status that an error occurred");
						break;
					}

					default:
					{
						// Display the status
						ROS_INFO_STREAM("[TEMPLATE GPIO EVENT TRIG.] gpiod_line_event_read returned an unrecognised status, return_flag =  " << returned_read_flag );
						break;
					}
				} // END OF: "switch (returned_read_flag)"
				break;
			}

			// Time out occurred
			case 0:
			{
				// Spin once so that this node can service the publishing
				// of this message, and any other callbacks that this
				// node has queued
				ros::spinOnce();
				break;
			}

			// Error occurred
			case -1:
			{
				// Display the status
				ROS_INFO("[TEMPLATE GPIO EVENT TRIG.] gpiod_line_event_wait returned the status that an error occurred");
				break;
			}

			default:
			{
				// Display the status
				ROS_INFO_STREAM("[TEMPLATE GPIO EVENT TRIG.] gpiod_line_event_wait returned an unrecognised status, return_flag =  " << returned_wait_flag );
				break;
			}
		} // END OF: "switch (returned_wait_flag)"
	} // END OF: "while (ros::ok())"

	// Close the GPIO chip
	gpiod_chip_close(chip);

	return 0;
}
