// Copyright (C) 2021, The University of Melbourne, Department of Electrical and Electronic Engineering (EEE)
//
// This file is part of ASClinic-System.
//
// See the root of the repository for license details.
//
// ----------------------------------------------------------------------------
//     _    ____   ____ _ _       _          ____            _
//    / \  / ___| / ___| (_)____ (_) ___    / ___| _   _ ___| |_ ___ ________
//   / _ \ \___ \| |   | | |  _ \| |/ __|___\___ \| | | / __| __/ _ \  _   _ \
//  / ___ \ ___) | |___| | | | | | | (_|_____|__) | |_| \__ \ ||  __/ | | | | |
// /_/   \_\____/ \____|_|_|_| |_|_|\___|   |____/ \__, |___/\__\___|_| |_| |_|
//                                                 |___/
//
// DESCRIPTION:
// Template node for polling the value of a GPIO pin at a fixed frequency
//
// ----------------------------------------------------------------------------





#include "ros/ros.h"
#include <ros/package.h>
#include "std_msgs/Int32.h"
#include "std_msgs/Bool.h"


#include <gpiod.h>
#include "asclinic_pkg/motor_ctrl.h"
#include "asclinic_pkg/pose_msg.h"
using namespace asclinic_pkg;
int current_task;
ros::Timer timer_debug;


float current_pose[3];
float pose_ref[3];
float s[2]; //s[0] = S_par , s[1] = S-perp
float x[0]; //x[0] = v, x[1] = omega


// Respond to subscriber receiving a message
void PoseRefSubscriberCallback(const pose_msg& msg)
{
	//ROS_INFO_STREAM("[MOTOR CRTL] pose_ref x_p: " << msg.x_p   << " y_p: " <<  msg.y_p  << " phi: " << msg.phi);
	pose_ref[0] = msg.x_p;
	pose_ref[1] = msg.y_p;
	pose_ref[2] = msg.phi;

}

// Respond to subscriber receiving a message
void CurrentPoseSubscriberCallback(const pose_msg& msg)
{
	//ROS_INFO_STREAM("[MOTOR CRTL] current_pose x_p: " << msg.x_p   << " y_p: " <<  msg.y_p  << " phi: " << msg.phi);
	current_pose[0] = msg.x_p;
	current_pose[1] = msg.y_p;
	current_pose[2] = msg.phi;
}

void current_task_callback(const std_msgs::Int32 msg)
{
    
    current_task = msg.data;
}

void timerCallback(const ros::TimerEvent&)
{
	
	static float counter = 0;

	counter = counter+ 0.5;
    
	timer_debug.stop();
	// > Set the period again (second argument is reset)
	timer_debug.setPeriod( ros::Duration(0.5), true);
	// > Start the timer again
	timer_debug.start();
//	ROS_INFO_STREAM("TIME IS: " << counter << " CURRENT POSE: " << current_pose[0] << " " << current_pose[1] << " " << current_pose[2]/3.1415*180 << "  POSE_REF: " << pose_ref[2]);
	//ROS_INFO_STREAM("TIME IS: " << counter << " SEC; CURRENT POSE: " << current_pose[2]/3.1415*180 << " DEG;    POSE_REF: " << pose_ref[2]/3.1415*180<<" DEG    CURRENT TASK: " << current_task);
}


int main(int argc, char* argv[])
{
	// Initialise the node
	ros::init(argc, argv, "motor_ctrl");
	ros::NodeHandle nodeHandle("~");
	ros::NodeHandle GlobalNodeHandle("/global");
	// Initialise a publisher
	ros::Publisher motor_ctrl_publisher = GlobalNodeHandle.advertise<motor_ctrl>("motor_ctrl", 10, false);
	// Initialise a subscriber
	ros::Subscriber pose_ref_subscriber = GlobalNodeHandle.subscribe("pose_reference", 1, PoseRefSubscriberCallback);
	ros::Subscriber current_pose_subscriber = GlobalNodeHandle.subscribe("current_pose", 1, CurrentPoseSubscriberCallback);
	ros::Subscriber task = GlobalNodeHandle.subscribe("current_task", 1, current_task_callback);
    timer_debug = nodeHandle.createTimer(ros::Duration(0.5), timerCallback, true);
	// Initialise a variable with loop rate for
	// polling the GPIO pin
	// > Input argument is the frequency in hertz, as a double
// 	ros::Rate loop_rate(10);

	while (ros::ok())
	{
	    /*Compute S-par and S-perp */
	    /*Different Logic for State 2 and 5 */
	    if (current_task == 2){ //driving +ve Y-axis
    	    /*TODO: Map S-par and S-perp to v and omega, Now random gain of 0.5*/
    	    x[0] = 0;
    	    x[1] = 2.5 * (pose_ref[2]-current_pose[2]);
    	    //ROS_INFO_STREAM("CT: " << current_task << "   v: " << x[0] << "   omega: " << x[1]);
	    }
	    
	    if (current_task == 3){ //driving +ve Y-axis
    	    s[0] = pose_ref[1] - current_pose[1];  //Y-axis
    	    s[1] = pose_ref[0] - current_pose[0] ; //X-axis
    	    /*TODO: Map S-par and S-perp to v and omega, Now random gain of 0.5*/
	    x[0] =  1.2*s[0];
	    x[1] =  -3*s[1];
	    }
	    if (current_task == 4) { 
	        x[0] = 0;
	        x[1] = 0;
	        }
	    if (current_task == 5){ //driving -ve Y-axis after turning
    	    x[0] = 0;
	        x[1] = 0;
	    }
	    
	    if (current_task == 6){ //driving -ve Y-axis after turning
	    
	    s[0] = pose_ref[1] - current_pose[1];  //Y-axis
    	    s[1] = pose_ref[0] - current_pose[0] ; //X-axis
    	    x[0] = 1.3*s[0]; //1.2
	        x[1] = 1.5*s[1]; //2
	    }
        if (current_task == 7){
	        x[0] = pose_ref[0] - current_pose[0];
	        x[1] = 3*(pose_ref[2] - current_pose[2]);
 	    }
 	    
 	    if (current_task == 8 || (current_task == 9)) {
 	        x[0] = 0;
 	        x[1] = 0;
 	    }
	    /* Publishing v and omega */
	    motor_ctrl msg = motor_ctrl();
		msg.v = x[0];
		msg.omega = x[1];
		motor_ctrl_publisher.publish(msg);
		
		ros::spinOnce();
// 		loop_rate.sleep();
	}

	return 0;
}
